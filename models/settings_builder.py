import yaml
import constants
from requests import get

class SettingsBuilder:
    def __init__(self):
        self.ip = get('https://api.ipify.org').content.decode('utf8')

        self.settings = yaml.safe_load(open("settings.yml"))
        server = self.settings["server"]

        self.host = server["host"]
        self.port = server["port"]
        self.test = server["test"]
        self.version = server["version"]

        environments = self.settings["environments"]
        local = environments["local"]
        local_name = local[constants.NAME]
        local_ip = local[constants.IP]
        local_debug = local[constants.DEBUG]
        local_storage = local[constants.STORAGE]
        local_proxy = local[constants.PROXY]

        develop = environments["develop"]
        develop_name = develop[constants.NAME]
        develop_ip = develop[constants.IP]
        develop_debug = develop[constants.DEBUG]
        develop_storage = develop[constants.STORAGE]
        develop_proxy = develop[constants.PROXY]

        prod = environments["prod"]
        prod_name = prod[constants.NAME]
        prod_ip = prod[constants.IP]
        prod_debug = prod[constants.DEBUG]
        prod_storage = prod[constants.STORAGE]
        prod_proxy = prod[constants.PROXY]

        if self.ip == develop_ip:
            self.name = develop_name
            self.debug = develop_debug
            self.storage = develop_storage
            self.proxy = develop_proxy
        elif self.ip == prod_ip:
            self.name = prod_name
            self.debug = prod_debug
            self.storage = prod_storage
            self.proxy = prod_proxy
        else:
            self.ip = local_ip
            self.name = local_name
            self.debug = local_debug
            self.storage = local_storage
            self.proxy = local_proxy

        self.settings["server"]["debug"] = self.debug