from datetime import datetime

class DateFilter:
    def __init__(self, request):
        self.start = datetime.strptime(request["start"], "%m/%d/%Y")
        self.end = datetime.strptime(request["end"], "%m/%d/%Y")

    def is_file_between_dates(self, filename):
        file_date = datetime.strptime(filename[:10], "%Y_%m_%d")
        return self.start <= file_date <= self.end
